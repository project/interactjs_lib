INTRODUCTION
------------

This is a module that allows the interactjs javascript library to be used via
Libraries API module.

For a full description of the module, visit the project page:
https://www.drupal.org/project/interactjs_lib

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/interactjs_lib

interactjs is an open source jQuery extension with source available on github.
Please post any bugs related to interactjs.js on github:
https://github.com/johnpolacek/interactjs.js/issues


REQUIREMENTS
------------
This module requires the following modules:

* Libraries API (https://drupal.org/project/libraries)


INSTALLATION
------------

1. Download the latest version of the interactjs library from:
https://github.com/taye/interact.js

2. Extract the library to a library directory named interactjsjs. For example if
you are using the default libraries location the js file will be at
sites/all/libraries/interactjsjs/interactjs.js

3. Remove unneeded files and folders from interactjsjs directory (optional).
You really only need to keep package.json, interactjs.min.js and interactjs.js
and optionally README.md LICENSE

4. Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.
